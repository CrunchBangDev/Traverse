from __future__ import annotations

import hashlib
import json
import os

import requests
from bs4 import BeautifulSoup

from config import CONFIG as config

if not config["url"] or not config["path"]:
  print("You must specify a URL and a path.",
    f"Run python {os.path.basename(__file__)} -h for details.")
  exit(1)
if not config["url"][-1] == "/":
  config["url"] = f"{config['url']}/"
if "http://" not in config["url"] and "https://" not in config["url"]:
  config["url"] = f"https://{config['url']}"
if not os.path.isdir(config["path"]):
  try:
    os.makedirs(config["path"])
  except IOError:
    print(f"Path does not exist and failed to create it: {config['path']}")
    exit(1)

if config["progressbar"]:
  # This is only imported on demand to respect people's right
  # to not run code from Google
  import progressbar

# Build Traverse class
class Traverse:
  def __init__(self, config: dict, *args, **kwargs):
    self.base_depth = config["url"].count("/") - 3
    config["depth"] += self.base_depth
    self.config = config
    self.headers = {
      "Accept-Encoding": "gzip, deflate",
      "User-Agent": ("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) "
                     "Gecko/20100101 Firefox/52.0")
    }

    if config["expand"] and not config["flat"]:
      print("WARNING! Archives will be expanded automatically. This "
            "combination of options should only be used with trusted sources!")

    self.manifest_file = os.path.join(config["path"], ".manifest")
    self.manifest = [Download()]
    self.cur_depth = 0

    if config["skip-cert-check"]:
      requests.packages.urllib3.disable_warnings()

    try:
      with open(self.manifest_file, "r") as f:
        self.manifest = f.read() or "[]"
        try:
          if self.manifest:
            # Some very basic attempts to clean the manifest
            while self.manifest[-1:] in ["\n", ","]:
              self.manifest = self.manifest[:-1]
            if self.manifest[0] != "[":
              self.manifest = f"[{self.manifest}]"
            self.manifest = [Download(x) for x in json.loads(self.manifest)]
        except Exception as e:
          print(f"Corrupted manifest: {self.manifest_file}: {e}")
          os.rename(self.manifest_file, f"{self.manifest_file}.corrupted")
          os._exit(1)
    except IOError:  # File doesn't exist
      f = open(self.manifest_file, "w")
      f.close()

  def traverse(self, branch: str = "") -> bool:
    if branch == "":
      print(f"Loading {self.config['url']}")
    page = requests.get(
      f"{self.config['url']}{branch}",
      headers=self.headers,
      verify=not self.config["skip-cert-check"]
    )
    if not page.ok:
      print(f"Bad response ({page.status_code}) getting directory {branch}")
      return False

    seen_files = [x.name for x in self.manifest if
                  x.path == os.path.join(self.config['path'], branch)]

    tree = BeautifulSoup(page.text, "html.parser")

    for node in tree.find_all("a"):
      node_href = node.get("href")
      if node_href[0:4] == "http":
        if not self.config["url"] in node_href:
          print(f"Ignoring off-site link: {node_href}")
          continue
        if len(node_href) < len(self.config["url"]):
          continue
      if self.config["url"] in node_href:
        node_href = node_href.replace(self.config["url"], "")
      if node_href[-1] == "/":
        # Let's not backtrack
        if node_href == "../" or node_href in self.config["url"]:
          continue
        node = f"{branch}{node_href}"
        if self.config["depth"] not in [False, -1]:
          if node.count("/") > self.config["depth"]:
            continue
        print(f"{'Going deeper! ' if node.count('/') > self.cur_depth else ''}"
              f"Reading {node}")
        self.cur_depth = node.count("/")
        self.traverse(node)
        continue
      # Make sure we're only following links to leaves at current depth
      if not (node_href == f"{branch}{node.contents[0]}"
              or node_href == node.contents[0]):
        continue
      manfile = None
      if node_href in seen_files:
        if self.config["assume-unchanged"]:
          continue
        manfile = [x for x in self.manifest if
                   x.source == f"{self.config['url']}{branch}{node_href}"][0]
      self.get_leaf(branch, node_href, manfile)

    if branch == "":
      f = open(self.manifest_file, "w")
      f.close()
      with open(self.manifest_file, "a") as f:
        for manfile in self.manifest:
          f.write(f"{json.dumps(manfile.__dict__)},")
      print("Done!")
      return True

  def get_stream(self, target: str) -> requests.Request:
    return requests.get(
      target,
      headers=self.headers,
      stream=True,
      verify=not self.config["skip-cert-check"]
    )

  def get_leaf(self,
               branch: str,
               leaf: str,
               manifest_file: Download = None) -> bool:
    save_path = os.path.join(self.config['path'], branch)
    F = Download({'name': leaf,
                  'path': save_path,
                  'source': f"{self.config['url']}{branch}{leaf}",
                  'peekpct': self.config['peekpct'],
                  'peeksize': self.config['peeksize']})

    if not os.path.exists(save_path):
      os.makedirs(save_path)

    r = self.get_stream(F.source)
    if not r.ok:
      print(f"Bad response ({r.status_code}) getting file {branch}{leaf}")
      return False

    F.dsize = int(r.headers.get('content-length'))

    F.peekhash = hashlib.md5()  # nosec (md5 is fine for file hashes)
    if manifest_file and F.dsize == manifest_file.dsize \
      and not self.config["assume-unchanged"]:
      F.peekpct = manifest_file.peekpct
      F.peeksize = manifest_file.peeksize
      peek_bytes = F.peeksize
      peek_hash = manifest_file.peekhash
    else:
      peek_bytes = int(F.peekpct * F.dsize / 100)
      if peek_bytes < F.peeksize:
        peek_bytes = F.peeksize
      if peek_bytes > F.dsize:
        peek_bytes = F.dsize
      peek_hash = ""
    peek_remainder = peek_bytes % self.config["chunksize"]
    F.peeksize = peek_bytes

    pbar = None
    if self.config['progressbar']:
      print()
      # Initialize progressbar
      pbar = progressbar.ProgressBar(
        widgets=[f"Getting file {branch}{leaf}: ",
                 progressbar.Counter(),
                 f"/{F.dsize} (",
                 progressbar.Percentage(),
                 ') ',
                 progressbar.Bar(),
                 progressbar.AdaptiveETA()], maxval=F.dsize
      ).start()
    else:
      print(f"Getting file {branch}{leaf} ({F.dsize } bytes)...")

    # Get the file
    downloaded = 0
    with open(os.path.join(save_path, F.lname + '.part'), 'wb') as f:
      for chunk in r.iter_content(chunk_size=self.config["chunksize"]):
        downloaded += len(chunk)
        if downloaded <= F.dsize:
          if pbar:
            pbar.update(downloaded)
        else:
          print("\nWarning: Exceeded advertised size! "
                f"{downloaded} > {F.dsize}")
          if pbar:
            pbar.update(F.dsize)
        if downloaded <= F.peeksize - (peek_remainder):
          F.peekhash.update(chunk)
        elif downloaded == (F.peeksize
                            + (self.config["chunksize"] - peek_remainder)):
          F.peekhash.update(chunk[0:peek_remainder])
        else:
          if not type(F.peekhash) == str:
            F.peekhash = F.peekhash.hexdigest()
            if F.peekhash == peek_hash:
              print("\nFile appears unchanged. Moving on...", end='')
              r.close()
              os.remove(os.path.join(save_path, F.lname + '.part'))
              return True
        f.write(chunk)
      r.close()
    if pbar:
      pbar.finish()
    if manifest_file:
      # Rebuild manifest without the superceded file
      self.manifest = [x for x in self.manifest if not
                       (x.peekhash == peek_hash and x.source == F.source)]
      if self.config["delete-superceded"]:
        F._lname = manifest_file._lname
        F.saved = True  # Locks in the file's local name
    os.rename(os.path.join(save_path, F.lname + '.part'),
              os.path.join(save_path, F.lname))
    F.saved = True

    # Append to manifest
    self.manifest.append(F)
    # Extract if requested
    if self.config["expand"]:
      F.extract()
    # Write to disk to prevent lost information from early termination
    with open(self.manifest_file, "a") as f:
        f.write(f"{json.dumps(F.__dict__)},")
    return True

class Download(object):
  def __init__(self, download: dict = {}, *args, **kwargs):
    # Deserialize imported file object automagically
    self.__dict__.update(download)
    # These properties always need to exist
    self.name = getattr(self, 'name', '')
    self.path = getattr(self, 'path', '')
    self.source = getattr(self, 'source', '')
    self.dsize = getattr(self, 'dsize', 0)
    # Populate other property values
    self.lname

  @property
  def lname(self) -> str:  # filename
    """ The local file name if saved, otherwise a dynamically generated one """
    if not getattr(self, 'saved', False) or not getattr(self, '_lname', ''):
      self._lname = self.get_unique_fname(self.path, self.name)
    return self._lname

  @property
  def lsize(self) -> int:  # bytes
    """ The size of the file if saved, otherwise 0 """
    try:
      return os.path.getsize(os.path.join(self.path, self.lname))
    except IOError:  # Probably not saved, don't care
      return 0

  def get_unique_fname(self, directory: str, filename: str) -> (str):
    """
    Determines a unique filename to avoid overwriting existing files

    Parameters:
      filename (str): The desired filename
      directory (str): Filesystem location at which to check for conflicts
    """
    if not os.path.exists(os.path.join(directory, filename)):
      return filename
    basename, ext = self.splitext(filename, extpartlim=2)
    i = 1
    while os.path.exists(os.path.join(directory, f"{basename} ({i}){ext}")):
      i += 1
    # Apply the filename determined by the previous step
    return f"{basename} ({i}){ext}"

  def splitext(self,
               filename: str,
               extlim: int = -1,
               extpartlim: int = None) -> (str, str):
    """
    Returns name and extension parts for a given filename.
    Basically os.path.splitext() except recursive and with options.

    Parameters:
      filename (str): The filename to split
      extlim (int): Limit the number of characters allowed in an extension
      extpartlim (int): Limit the number of extension parts returned
    """
    ext = ""
    extpartlim = extpartlim or filename.count('.')
    # Avoid infinite recursion
    if extlim >= len(filename):
      extlim -= 1
    while '.' in filename[-extlim:] and ext.count('.') < extpartlim:
      filename, ext2 = os.path.splitext(filename)
      # Avoid infinite recursion
      if not ext2:
        break
      ext = ext2 + ext
    return filename, ext

  def extract(self, file: str = "", loop: bool = True) -> bool:
    out_files = []
    if not file:
      file = self.lname
    f_base, f_ext = os.path.splitext(file)

    # ZIP archives
    if f_ext == ".zip":
      import zipfile
      print(f"Expanding ZIP archive {file}.")
      try:
        with zipfile.ZipFile(os.path.join(self.path, file)) as zip:
          # testzip() returns None or name of first bad file
          if zipfile.ZipFile.testzip(zip) is not None:
            print("Malformed ZIP or contents corrupted! Unable to process.")
            return False
          if self.config["flat"]:
            # Not using extractall() because we don't want a tree structure
            for member in zip.infolist():
              member = self.get_unique_fname(self.path, member)
              if self.config["flat"]:
                zip.extract(member, self.path)
              else:
                zip.extract(member)
              out_files.append(str(member))
          else:
            zip.extractall(self.path)
          # Delete the zip file now that we have its contents
        os.remove(os.path.join(self.path, file))
      except Exception as e:
        print(f"Unable to expand ZIP archive {file}: {e}")
        return False

    # GZIP compression
    elif f_ext == ".gz":
      import gzip
      import shutil
      print(f"Expanding GZIP compressed file {file}.")
      try:
        out_fname = self.get_unique_fname(self.path, f_base)
        with gzip.open(os.path.join(self.path, file), "rb") as f_in, \
             open(os.path.join(self.path, out_fname), "wb") as f_out:
          shutil.copyfileobj(f_in, f_out)
        out_files.append(out_fname)
        # Delete the gz file now that we have its contents
        os.remove(os.path.join(self.path, file))
      except Exception as e:
        print(f"Unable to expand GZIP file {file}: {e}")
        return False

    # TAR archives
    elif f_ext == ".tar":
      import tarfile
      print(f"Expanding TAR archive {file}.")
      try:
        with tarfile.open(os.path.join(self.path, file), "r") as tar:
          if self.config["flat"]:
            # Not using extractall() because we don't want a tree structure
            for member in tar.getmembers():
              if member.isreg():
                if self.config["flat"]:
                  # Strip any path information from members
                  member.name = self.get_unique_fname(
                    self.path,
                    os.path.basename(member.name)
                  )
                tar.extract(member, self.path)
                out_files.append(member.name)
        # Delete the tar file now that we have its contents
        os.remove(os.path.join(self.path, file))
      except Exception as e:
        print(f"Unable to expand TAR archive {file}: {e}")

    # The file is not compressed or archived, or not a supported format
    else:
      return

    if not loop:
      return

    # Iterate back through, in case of layered archives or
    # compressed archives (e.g. example.tar.gz)
    for file in out_files:
      # Set loop switch to False to avoid creating blackhole
      self.extract(file, False)


if __name__ == "__main__":
  try:
    t = Traverse(config)
    t.traverse()
  except KeyboardInterrupt:
    print("\nBye!")
  except Exception as e:
    print(f"\nSomething went wrong: {e}")
    raise
