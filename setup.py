import setuptools

try:
  with open("README.md", "r") as f:
    long_description = f.read()

  with open("VERSION", "r") as f:
    version = f.read()

  with open("requirements.txt", "r") as f:
    requirements = f.readlines()
except FileNotFoundError:
  long_description = "???"
  requirements = []
  version = "???"

setuptools.setup(
  name="Traverse",
  version=version,
  author="Caleb White",
  author_email="cmwhite@pm.me",
  description="A kind of strongly typed config wrangler",
  long_description=long_description,
  long_description_content_type="text/markdown",
  url="https://gitlab.com/CrunchBangDev/Traverse",
  packages=setuptools.find_packages(),
  classifiers=[
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: GNU General Public License v3 or later "
    "(GPLv3+)",
    "Operating System :: OS Independent",
  ],
  python_requires='>=3.8',
  install_requires=requirements,
)
