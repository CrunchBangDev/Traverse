# Traverse
A tool to traverse web directories and download all files contained in them.

Options to auto-expand archives and compressed files.

## Setup
```bash
pip install git+https://gitlab.com/CrunchBangDev/Traverse.git
```
Requires Python 3.8.0+. Please don't open issues for old Python versions.

## Usage:
```bash
python traverse.py url path [depth] [chunksize] [peeksize] [peekpct] [-h] [--expand] [--flat]
                   [--skip-cert-check] [--assume-unchanged] [--delete-superceded] [--progressbar]
```
You can also run it with no arguments and the initial configuration wizard will help you figure things out.

Note: In order to use the `--progressbar` flag, you will have to `pip install progressbar` separately. This is because the package is from Google and therefore many people might not trust it, so I didn't feel good having it in `requirements.txt`.

## Examples:
```bash
python traverse.py www.example.com /path/to/save/location 5 4096 --expand --flat --skip-cert-check
python traverse.py http://old-releases.ubuntu.com/releases oldbuntu --assume-unchanged
```

Download all the things!
