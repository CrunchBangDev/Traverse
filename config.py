from conf import Conf
from conf import ConfItem as CI

with open('VERSION', 'r') as f:
  VERSION = f.read()

abstract_conf = (
    CI("url",
       "URL to download from.",
       str,
       required=True,
       wizard="Define a URL to target"),
    CI("path",
       "Path to directory to save items into.",
       str,
       required=True,
       wizard="Specify a path to save things to"),
    CI("depth",
       "Maximum depth of directories to traverse (0 for infinite).",
       int,
       wizard="Specify a search depth to stop at"),
    CI("chunksize",
       "Chunk size for writing to disk (default is 4096).",
       int, 4096,
       wizard="Specify a custom chunk size"),
    CI("peeksize",
       "How many bytes of a file to read to determine its uniqueness.",
       int, 32768,
       wizard=("Specify a minimum number of bytes to read to determine file "
               "uniqueness (default is 32768)")),
    CI("peekpct",
       "Percentage of a file to read to determine its uniqueness.",
       int, 2,
       wizard=("What is the minimum percentage of a file that should be "
               "evaluated for uniqueness (default is 2)?")),
    CI("--expand",
       "Automatically expand archives and compressed files.",
       wizard="Do you want to expand archives?"),
    CI("--flat",
       "Ignore archive directory structure.",
       default=True,
       wizard="Ignore directory structure when expanding archives?"),
    CI("--skip-cert-check",
       "Skip certificate checks when making HTTPS connections.",
       wizard=("Do you want to skip certificate validation when making HTTPS "
               "connections?")),
    CI("--assume-unchanged",
       "Make download decisions solely based on filenames and paths.",
       default=True,
       wizard="Do you want to assume downloaded files will never change?"),
    CI("--delete-superceded",
       "Delete files if a newer copy has been downloaded.",
       default=True,
       wizard="Do you want to remove old copies of files?"),
    CI("--progressbar",
       "Display a progress bar with estimated completion time.",
       default=False,
       wizard="Do you want fancy progress bars?"),
)

CONFIG = Conf(abstract_conf=abstract_conf,
              prog="Traverse",
              version=VERSION,
              description="""
A tool to traverse web directories and download all files contained in them.
""").config

# TODO: Add epilog once Conf is updated to support it
# epilog = """\
# Note: If you do not specify a protocol,
# https:// will be prepended to your URL.

# Download all the things!"""
